import os
import shutil
import random

days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
periods = ['morning', 'evening']

for day in days:
    if os.path.exists(day):
        shutil.rmtree(day)
    os.mkdir(day)
    for period in periods:
        os.mkdir(os.path.join(day, period))
        with open(os.path.join(day, period, 'Solutions.csv'), 'w') as f:
            time_of_computation = random.randint(0, 1000)
            f.writelines([" Model; Output value; Time of computation; \n",
                          f" {random.choice('ABC')} ; {random.randint(0, 1000)} ; {time_of_computation}s; "])

