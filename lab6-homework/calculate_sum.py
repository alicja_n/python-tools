import os

days = set(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])
periods = ['morning', 'evening']

existing_days = set(os.listdir('.')) & days
sum_of_times = 0
for day in existing_days:
    for period in periods:
        path = os.path.join(day, period, 'Solutions.csv')
        if os.path.isfile(path):
            with open(path, 'r') as f:
                next(f)
                line = next(f)
                time_of_computation = int(line.split(';')[-2].strip(' s'))
                sum_of_times += time_of_computation
print(f"Total time of computation: {sum_of_times}")
